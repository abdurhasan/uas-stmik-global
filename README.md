[![Build Status](https://travis-ci.org/joemccann/dillinger.svg)](https://gitlab.com/abdurhasan/uas-stmik-global)

UAS mobile progamming STMIK Bina Sarana Global

  - using hybrid technology : cordova
  - cross platform IOS - Android
  - using cloud-based backend ( deployed on heroku ) :  https://uasstmikglobal.herokuapp.com/
  

#### Building for the app
```sh   
$ npm i -g cordova   // installing cordova globally
```

For Android release .apk ( debug ):
```sh
$ cordova add platform android
$ cordova build android
$ apk file at : /platforms/android/app/build/outputs/apk/app-debug.apk
```
Browser
```sh
$  cordova add platform browser
$  cordova run browser
```
