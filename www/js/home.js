const iptToggleModal = document.querySelector('#ipt-toggle-modal');
const modal = document.querySelector('.modal');
const btnCancelTask = document.querySelector('.btn-cancel-task');
const BASE_URL = 'https://uasstmikglobal.herokuapp.com/'
const dataTransfer = []
let idTransfer = ''
const stateData = {
  accountNoDestination: '',
  nominal: 0,
  bank: '',
  accountNoOrigin: '',
  description: ''
}

btnCancelTask.addEventListener('click', () => {
  iptToggleModal.checked = false;
  modal.reset();
  document.querySelectorAll('.task').forEach(task => {
    task.classList.remove('swipe-right');
  });
});


$('#submitTransfer').submit(async e => {
  e.preventDefault();

  if (checkProperties(stateData)) {

    iptToggleModal.checked = false;
    modal.reset();

    await bootstrap()

    try {
      await axios.post(BASE_URL + 'transferDomestic', stateData)
    } catch (error) {
      alert('Transfer failed ' + error.message)
      window.relaod()
    }

  }

})

function editTransfer(evt, el) {

  document.querySelectorAll('input').forEach(item => {

    if (item.value == priority) {
      item.checked = true;
    }
  });

  // iptToggleModal.checked = true;
  document.querySelectorAll('.task').forEach(item => {
    item.classList.add('swipe-right');
  });


}

function checkProperties(obj) {
  let queue = true
  Object.values(obj).map(snap => {
    if (!snap) queue = false
  })
  return queue;
}

async function removeTask(evt, el) {
  const btn = el == undefined ? this : el;
  const task = btn.parentNode.parentNode.parentNode;
  let taskStatus = task.querySelector('.task-status');
  taskStatus.innerText = 'Task removed';

  task.classList.add('-is-removed');

  setTimeout(() => {
    task.classList.add('swipe-right');
  }, 500);

  setTimeout(() => {
    task.parentElement.removeChild(task);
  }, 1000)

  await axios.get(BASE_URL + 'transferDomestic/delete/' + $(el).attr('data-id'))
    .catch(_ => window.reload())
}

async function getData() {
  const data = await axios.get(BASE_URL + 'transferDomestic').then(snap => snap.data)
  dataTransfer.push(...data)

}

function renderData() {

  dataTransfer.map(datum => {
    const { id, amount, responseMsg, cdt, code, currency } = datum
    const transferTo = datum.user.accountNo
    const bank = datum.user.bankName
    const accountName = datum.user.name
    const totalAmount = `${currency} ${amount}`
    $('.tasks').append(`
        <div class="task" data-index="0" style="transition-delay: 0s;">
        <div class="task-header">
            <div class="left-side">
              <span class="task-title">${id} | ${responseMsg}</span>
            </div>
            <div class="right-side">
            <!-- <div class="btn-edit-task" title="Edit task" onclick="editTransfer()">
                  <ion-icon name="create" role="img" class="hydrated" aria-label="create"></ion-icon>
                </div> -->
              
              <div class="btn-remove-task" title="Remove task" onclick="removeTask(event, this)" data-id="${id}">
                <ion-icon name="trash" role="img" class="hydrated" aria-label="trash"></ion-icon>
              </div>
            </div>
          </div>
        <div class="task-body"><span class="task-description">Transfer To : ${transferTo}</span></div>
        <div class="task-body"><span class="task-description">Bank Destination: ${bank}</span></div>
        <div class="task-body"><span class="task-description">Account Destination : ${code}</span></div>
        <div class="task-body"><span class="task-description">Account Destination Name : ${accountName}</span></div>
        <div class="task-body"><span class="task-description">Transfer Nominal : ${totalAmount}</span></div>
        <div class="task-footer"><span class="task-status">Task completed</span>
            <div class="task-timestamp">${moment(cdt).format("DD MMMM YYYY")}</div>
        </div>
    </div>
        `)


  })

}

function changeState(stateKey) {
  const newValue = $(`#${stateKey}`).val()
  stateData[stateKey] = newValue
  if (stateKey == 'nominal' && isNaN(newValue)) {
    $('#nominal').val('')
    $('#nominal').attr('placeholder', 'Must be a number')
    stateData['nominal'] = null
  }
}

async function bootstrap() {
  const userData = localStorage.getItem('userData')
  Spinner();
  Spinner.show();

  if (!userData) {
    window.location = "index.html";
  }

  const userDoc = JSON.parse(userData)
  const { name, accountNo } = userDoc
  const balance = await axios.get(BASE_URL + 'balance/' + accountNo).then(snap => snap.data.balance)


  $('#userAccount').text(name)
  $('#userBalance').text('Your Balance : ' + balance)

  await getData()
  Spinner.hide()
  renderData()

}

bootstrap()